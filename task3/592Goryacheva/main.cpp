#include <common/Application.hpp>
#include <common/LightInfo.hpp>
#include <common/Mesh.hpp>
#include <common/ShaderProgram.hpp>
#include <common/Texture.hpp>
#include <common/Camera.hpp>


#include <iostream>
#include <sstream>
#include <vector>


class SampleApplication : public Application {
public:
	MeshPtr _teapot;
	MeshPtr _ground;

	MeshPtr _cube;
	MeshPtr _golub;

	MeshPtr _marker; //Маркер для источника света

	//Идентификатор шейдерной программы
	ShaderProgramPtr _commonShader;
	ShaderProgramPtr _markerShader;

	//Переменные для управления положением одного источника света
	float _lr = 3.0;
	float _phi = 0.0;
	float _theta = glm::pi<float>() * 0.25f;

	
	std::vector<glm::vec3> _positionsVec3;
	std::vector<glm::vec3> _positionsVec3Cube;
	std::vector<glm::vec4> _positionsVec4;

	DataBufferPtr _bufVec3;
    	DataBufferPtr _bufVec4;	

	TexturePtr _bufferTexCulled;
    	TexturePtr _bufferTexAll;

	GLuint _TF; //Объект для хранения настроект Transform Feedback
    	GLuint _cullVao; //VAO, который хранит настройки буфера для чтения данных во время Transform Feedback
    	GLuint _tfOutputVbo; //VBO, в который будут записываться смещения моделей после отсечения

    	GLuint _query; //Переменная-счетчик, куда будет записываться количество пройденных отбор моделей
	ShaderProgramPtr _cullShader;


	
	const unsigned int K = 1000;
	const unsigned int KCube = 100;
	const float size = 200.0f;
	glm::mat4 matrix = glm::mat4(1.0f);

	float _speedOfCamera = 1.;

	LightInfo _light;

	TexturePtr _treeTexture;
	TexturePtr _groundTexture;

	TexturePtr _cubeTexture;
	TexturePtr _golubTexture;

	GLuint _groundSampler;
	GLuint _treeSampler;
	GLuint _cubeSampler;
	GLuint _golubSampler;

	SampleApplication()
	{
		_cameraMover = std::make_shared<FreeCameraMover>();
		_cameraMover->setSpeed( _speedOfCamera );
	}


	float frand()
    	{
        	return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
    	}	

	void makeScene() override
	{
		Application::makeScene();

		//=========================================================
		//Создание и загрузка мешей		

		_cube = makeCube(2.0f);
       		_cube->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 2.0f)));


		_ground = makeGroundPlane( -1000, 10 );
		_ground->setModelMatrix( glm::translate( glm::mat4( 1.0f ), glm::vec3( 0.0f, 0.0f, -0.0001f ) ) );

		const std::string modelName1 = "592GoryachevaData2/models/golub.obj";

        	_golub = loadFromFile(modelName1);
		const std::string modelName2 = "592GoryachevaData2/models/teapot.obj";

        	_teapot = loadFromFile(modelName2);
		for (unsigned int i = 0; i < K; i++)
       		{
          		_positionsVec3.push_back(glm::vec3(frand() * size - 0.5 * size, frand() * size - 0.5 * size, 0.0));
			_positionsVec4.push_back(glm::vec4(_positionsVec3.back(), 0.0));
        	}

		for (unsigned int i = 0; i < KCube; i++)
       		{
          		_positionsVec3Cube.push_back(glm::vec3(frand() * size/2 - 0.5 * size, frand() * size/2 - 0.5 * size/2, 0.0));
		}
		//Создаем буферы без выравнивания (_bufVec3) и с выравниванием (_bufVec4)

        	_bufVec3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        	_bufVec3->setData(_positionsVec3.size() * sizeof(float) * 3, _positionsVec3.data());

        	_bufVec4 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        	_bufVec4->setData(_positionsVec4.size() * sizeof(float) * 4, _positionsVec4.data());

		glGenBuffers(1, &_tfOutputVbo);
        	glBindBuffer(GL_ARRAY_BUFFER, _tfOutputVbo);
        	glBufferData(GL_ARRAY_BUFFER, _positionsVec3.size() * sizeof(float) * 3, 0, GL_STREAM_DRAW);
	
        	glBindBuffer(GL_ARRAY_BUFFER, 0);

		//Создаем текстурный буфер и привязываем к нему буфер без выравнивания
        	_bufferTexAll = std::make_shared<Texture>(GL_TEXTURE_BUFFER);
        	_bufferTexAll->bind();
        	glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F_ARB, _bufVec3->id());
        	_bufferTexAll->unbind();
	
        	//Создаем текстурный буфер и привязываем к нему буфер без выравнивания
        	_bufferTexCulled = std::make_shared<Texture>(GL_TEXTURE_BUFFER);
        	_bufferTexCulled->bind();
        	glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F_ARB, _tfOutputVbo);
        	_bufferTexCulled->unbind();

        	//----------------------------

		//Привязываем SSBO к 0й точке привязки (требуется буфер с выравниванием)
        	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, _bufVec4->id());


		_marker = makeSphere( 0.1f );


		//=========================================================
		//Инициализация шейдеров

		_commonShader = std::make_shared<ShaderProgram>( "592GoryachevaData2/shaders/instancingSSBO.vert", "592GoryachevaData2/shaders/common.frag" );
		_markerShader = std::make_shared<ShaderProgram>( "592GoryachevaData2/shaders/marker.vert", "592GoryachevaData2/shaders/marker.frag" );


		_cullShader = std::make_shared<ShaderProgram>();

        	ShaderPtr vs = std::make_shared<Shader>(GL_VERTEX_SHADER);
        	vs->createFromFile("592GoryachevaData2/shaders/cull.vert");
        	_cullShader->attachShader(vs);

        	ShaderPtr gs = std::make_shared<Shader>(GL_GEOMETRY_SHADER);
        	gs->createFromFile("592GoryachevaData2/shaders/cull.geom");
        	_cullShader->attachShader(gs);


		//Выходные переменные, которые будут записаны в буфер
        	const char* attribs[] = { "position" };
        	glTransformFeedbackVaryings(_cullShader->id(), 1, attribs, GL_SEPARATE_ATTRIBS);

        	_cullShader->linkProgram();

        	//----------------------------

        	//VAO, который будет поставлять данные для отсечения
        	glGenVertexArrays(1, &_cullVao);
        	glBindVertexArray(_cullVao);

        	glEnableVertexAttribArray(0);
        	glBindBuffer(GL_ARRAY_BUFFER, _bufVec3->id());
        	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	
        	glBindVertexArray(0);
	
        	//----------------------------
	
        	//Настроечный объект Transform Feedback
        	glGenTransformFeedbacks(1, &_TF);
        	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, _TF);
        	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, _tfOutputVbo);
        	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);

        	//----------------------------

	        glGenQueries(1, &_query);

		//=========================================================
		//Инициализация значений переменных освещения
		_light.position = glm::vec3( glm::cos( _phi ) * glm::cos( _theta ), glm::sin( _phi ) * glm::cos( _theta ), glm::sin( _theta ) ) * _lr;
		_light.ambient = glm::vec3( 0.2, 0.2, 0.2 );
		_light.diffuse = glm::vec3( 0.8, 0.8, 0.8 );
		_light.specular = glm::vec3( 1.0, 1.0, 1.0 );

		//=========================================================
		//Загрузка и создание текстур
		_treeTexture = loadTexture( "592GoryachevaData2/images/tree4.jpg" );
		_groundTexture = loadTexture( "592GoryachevaData2/images/earth_groundwas.jpg" );


		_cubeTexture = loadTexture( "592GoryachevaData2/images/cube.jpg" );
		_golubTexture = loadTexture( "592GoryachevaData2/images/golub.jpg" );


		//=========================================================
		//Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
		glGenSamplers( 1, &_groundSampler );
		glSamplerParameteri( _groundSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
		glSamplerParameteri( _groundSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		glSamplerParameteri( _groundSampler, GL_TEXTURE_WRAP_S, GL_REPEAT );
		glSamplerParameteri( _groundSampler, GL_TEXTURE_WRAP_T, GL_REPEAT );

		glGenSamplers( 1, &_treeSampler );
		glSamplerParameteri( _treeSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
		glSamplerParameteri( _treeSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		glSamplerParameteri( _treeSampler, GL_TEXTURE_WRAP_S, GL_REPEAT );
		glSamplerParameteri( _treeSampler, GL_TEXTURE_WRAP_T, GL_REPEAT );
		glSamplerParameteri( _treeSampler, GL_TEXTURE_WRAP_R, GL_REPEAT );


		glGenSamplers( 1, &_cubeSampler );
		glSamplerParameteri( _cubeSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
		glSamplerParameteri( _cubeSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		glSamplerParameteri( _cubeSampler, GL_TEXTURE_WRAP_S, GL_REPEAT );
		glSamplerParameteri( _cubeSampler, GL_TEXTURE_WRAP_T, GL_REPEAT );


		glGenSamplers( 1, &_golubSampler );
		glSamplerParameteri( _golubSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
		glSamplerParameteri( _golubSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		glSamplerParameteri( _golubSampler, GL_TEXTURE_WRAP_S, GL_REPEAT );
		glSamplerParameteri( _golubSampler, GL_TEXTURE_WRAP_T, GL_REPEAT );
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos( ImVec2( 0, 0 ), ImGuiSetCond_FirstUseEver );
		if( ImGui::Begin( "Task3", NULL, ImGuiWindowFlags_AlwaysAutoResize ) ) {
			if( ImGui::CollapsingHeader( "Camera settings" ) ) {
				ImGui::SliderFloat( "Speed of Camera", &_speedOfCamera, 0., 10. );
			}
		}
		ImGui::End();
	}

	void draw() override
	{
		//Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize( _window, &width, &height );

		glViewport( 0, 0, width, height );

		//Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClearColor( 1.0f, 1.0f, 1.0f, 1.0f );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		cull(_cullShader);


		_light.position = glm::vec3( glm::cos( _phi ) * glm::cos( _theta ), glm::sin( _phi ) * glm::cos( _theta ), glm::sin( _theta ) ) * _lr * 10.f;
		glm::vec3 lightPosCamSpace = glm::vec3( _camera.viewMatrix * glm::vec4( _light.position, 1.0 ) );


		_cameraMover->setSpeed( _speedOfCamera );
		

		GLuint primitivesWritten;
        	glGetQueryObjectuiv(_query, GL_QUERY_RESULT, &primitivesWritten);

		//Подключаем шейдер		
		_commonShader->use();

		//Загружаем на видеокарту значения юниформ-переменных
		_commonShader->setMat4Uniform( "viewMatrix", _camera.viewMatrix );
		_commonShader->setMat4Uniform( "projectionMatrix", _camera.projMatrix );

		_commonShader->setVec3Uniform( "light.pos", lightPosCamSpace ); //копируем положение уже в системе виртуальной камеры
		_commonShader->setVec3Uniform( "light.La", _light.ambient );
		_commonShader->setVec3Uniform( "light.Ld", _light.diffuse );
		_commonShader->setVec3Uniform( "light.Ls", _light.specular );


		glActiveTexture( GL_TEXTURE0 );  //текстурный юнит 0
		glBindSampler( 0, _groundSampler );
		_groundTexture->bind();
		_commonShader->setIntUniform( "diffuseTex", 0 );
		{
			_commonShader->setMat4Uniform( "modelMatrix", _ground->modelMatrix() );
			_commonShader->setMat3Uniform( "normalToCameraMatrix", glm::transpose( glm::inverse( glm::mat3( _camera.viewMatrix * _ground->modelMatrix() ) ) ) );

			
			_ground->draw();			



		}

		glActiveTexture( GL_TEXTURE0 );  //текстурный юнит 0
		glBindSampler( 0, _treeSampler );
		_treeTexture->bind();
		_commonShader->setIntUniform( "treeTex", 0 );
		

		glActiveTexture(GL_TEXTURE1);
		_bufferTexCulled->bind();
		_commonShader->setIntUniform("texBuf", 1);
		
		{
			unsigned int ssboIndex = glGetProgramResourceIndex(_commonShader->id(), GL_SHADER_STORAGE_BLOCK, "Positions");
                	glShaderStorageBlockBinding(_commonShader->id(), ssboIndex, 0); //0я точка привязки
			_commonShader->setMat4Uniform( "modelMatrix", glm::mat4(1.0) );
			_commonShader->setMat3Uniform( "normalToCameraMatrix", glm::transpose( glm::inverse( glm::mat3( _camera.viewMatrix) ) ) );
			
			_teapot->drawInstanced(primitivesWritten);
		}		

	/*		glActiveTexture( GL_TEXTURE0 );  //текстурный юнит 0
		glBindSampler( 0, _cubeSampler );
		_cubeTexture->bind();
		_commonShader->setIntUniform( "cubeTex", 0 );

		{
			_commonShader->setMat4Uniform( "modelMatrix", _cube->modelMatrix() );
			_commonShader->setMat3Uniform( "normalToCameraMatrix", glm::transpose( glm::inverse( glm::mat3( _camera.viewMatrix * _cube->modelMatrix() ) ) ) );

			_cube->draw();
		}   */
		
		for (unsigned int i = 0; i < KCube; i++)
       		{
			glActiveTexture( GL_TEXTURE0 );  //текстурный юнит 0
			glBindSampler( 0, _cubeSampler );
			_cubeTexture->bind();
			_commonShader->setIntUniform( "cubeTex", 0 );

			{
				_commonShader->setMat4Uniform( "modelMatrix", glm::translate(_cube->modelMatrix(),-_positionsVec3Cube[i]) );
				_commonShader->setMat3Uniform( "normalToCameraMatrix", glm::transpose( glm::inverse( glm::mat3( _camera.viewMatrix * _cube->modelMatrix() ) ) ) );

				_cube->draw();
			}
		}

		glActiveTexture( GL_TEXTURE0 );  //текстурный юнит 0
		glBindSampler( 0, _golubSampler );
		_golubTexture->bind();
		_commonShader->setIntUniform( "golubTex", 0 );

		{
			_commonShader->setMat4Uniform("modelMatrix", glm::translate(glm::scale(glm::mat4(1.0), glm::vec3(0.2, 0.2, 0.2)), glm::vec3(0.0f, 0.0f, 0.0f)));
                	_commonShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * glm::mat4(1.0)))));


			_golub->draw();
		}

		//Рисуем маркеры для всех источников света		
		{
			_markerShader->use();

			_markerShader->setMat4Uniform( "mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate( glm::mat4( 1.0f ), _light.position ) );
			_markerShader->setVec4Uniform( "color", glm::vec4( _light.diffuse, 1.0f ) );
			_marker->draw();
		}
		//Отсоединяем сэмплер и шейдерную программу
		glBindSampler( 0, 0 );
		glUseProgram( 0 );
	}

	void cull(const ShaderProgramPtr& shader)
    	{
    		shader->use();

        	shader->setFloatUniform("time", static_cast<float>(glfwGetTime()));

        	glEnable(GL_RASTERIZER_DISCARD);

        	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, _TF);

        	glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, _query);

        	glBeginTransformFeedback(GL_POINTS);

        	glBindVertexArray(_cullVao);
        	glDrawArrays(GL_POINTS, 0, _positionsVec3.size());

        	glEndTransformFeedback();

        	glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);

        	glDisable(GL_RASTERIZER_DISCARD);
    	}	
		
};



int main()
{
	SampleApplication app;
	app.start();

	return 0;
}
