# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/kate/katat/gl_tasks2019/task3/592Goryacheva/common/Application.cpp" "/home/kate/katat/gl_tasks2019/task3/592Goryacheva/CMakeFiles/592Goryacheva2.dir/common/Application.cpp.o"
  "/home/kate/katat/gl_tasks2019/task3/592Goryacheva/common/Camera.cpp" "/home/kate/katat/gl_tasks2019/task3/592Goryacheva/CMakeFiles/592Goryacheva2.dir/common/Camera.cpp.o"
  "/home/kate/katat/gl_tasks2019/task3/592Goryacheva/common/DebugOutput.cpp" "/home/kate/katat/gl_tasks2019/task3/592Goryacheva/CMakeFiles/592Goryacheva2.dir/common/DebugOutput.cpp.o"
  "/home/kate/katat/gl_tasks2019/task3/592Goryacheva/common/Framebuffer.cpp" "/home/kate/katat/gl_tasks2019/task3/592Goryacheva/CMakeFiles/592Goryacheva2.dir/common/Framebuffer.cpp.o"
  "/home/kate/katat/gl_tasks2019/task3/592Goryacheva/common/Mesh.cpp" "/home/kate/katat/gl_tasks2019/task3/592Goryacheva/CMakeFiles/592Goryacheva2.dir/common/Mesh.cpp.o"
  "/home/kate/katat/gl_tasks2019/task3/592Goryacheva/common/ShaderProgram.cpp" "/home/kate/katat/gl_tasks2019/task3/592Goryacheva/CMakeFiles/592Goryacheva2.dir/common/ShaderProgram.cpp.o"
  "/home/kate/katat/gl_tasks2019/task3/592Goryacheva/common/Texture.cpp" "/home/kate/katat/gl_tasks2019/task3/592Goryacheva/CMakeFiles/592Goryacheva2.dir/common/Texture.cpp.o"
  "/home/kate/katat/gl_tasks2019/task3/592Goryacheva/main.cpp" "/home/kate/katat/gl_tasks2019/task3/592Goryacheva/CMakeFiles/592Goryacheva2.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLEW_STATIC"
  "GLFW_DLL"
  "GLM_FORCE_DEPTH_ZERO_TO_ONE"
  "GLM_FORCE_PURE"
  "GLM_FORCE_RADIANS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "external/glew-1.13.0/include"
  "external/GLM"
  "external/SOIL/src/SOIL2"
  "external/Assimp/include"
  "external/GLFW/include"
  "external/imgui"
  "task3/592Goryacheva"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/kate/katat/gl_tasks2019/external/glew-1.13.0/build/cmake/CMakeFiles/glew_s.dir/DependInfo.cmake"
  "/home/kate/katat/gl_tasks2019/external/SOIL/CMakeFiles/soil.dir/DependInfo.cmake"
  "/home/kate/katat/gl_tasks2019/external/Assimp/code/CMakeFiles/assimp.dir/DependInfo.cmake"
  "/home/kate/katat/gl_tasks2019/external/imgui/CMakeFiles/imgui.dir/DependInfo.cmake"
  "/home/kate/katat/gl_tasks2019/external/Assimp/contrib/irrXML/CMakeFiles/IrrXML.dir/DependInfo.cmake"
  "/home/kate/katat/gl_tasks2019/external/GLFW/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
